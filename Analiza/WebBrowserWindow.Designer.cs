﻿namespace Analiza
{
    partial class WebAnalyserWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.adresArea = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // adresArea
            // 
            this.adresArea.Location = new System.Drawing.Point(13, 13);
            this.adresArea.Name = "adresArea";
            this.adresArea.Size = new System.Drawing.Size(391, 20);
            this.adresArea.TabIndex = 0;
            this.adresArea.KeyUp += new System.Windows.Forms.KeyEventHandler(this.adresArea_KeyUp);
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(410, 11);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 1;
            this.submitButton.Text = "Otwórz";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(12, 40);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(472, 285);
            this.textBox.TabIndex = 2;
            this.textBox.Text = "";
            // 
            // WebBrowserWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 337);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.adresArea);
            this.Name = "WebBrowserWindow";
            this.Text = "PageStat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox adresArea;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.RichTextBox textBox;
    }
}

