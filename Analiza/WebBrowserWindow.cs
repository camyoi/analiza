﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace Analiza
{
    public partial class WebAnalyserWindow : Form
    {
        public WebAnalyserWindow()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            analise();
        }

        private void adresArea_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                analise();
            }
        }

        /// <summary>
        /// Wykonuje analize podanej strony www oraz wyswietla statystyke slow kluczowych
        /// </summary>
        private void analise()
        { 
            try
            {
                Uri uri = UriHelper.uriCheck(adresArea.Text);
                String htmlString = WebRequestHelper.getPageHtml(uri);
                HtmlAgilityPack.HtmlDocument htmlDoc = HtmlHelper.getHtmlDocumentFromHtml(htmlString);
                List<String> keyWords = HtmlHelper.getKeywordsMetaTagList(htmlDoc);
                HtmlNode bodyNode = HtmlHelper.getBody(htmlDoc);
                Dictionary<String, Int32> keywordCount = HtmlHelper.countKeys(keyWords, bodyNode);
                String responseText = generateResponseText(keywordCount);
                textBox.Clear();
                textBox.Text = responseText;
            }
            catch (Exception ex)
            {
                textBox.Text = ex.Message;
            }
        }

        /// <summary>
        /// Generuje tresc do wyswietlenia
        /// </summary>
        /// <param name="keywordCount">Slownik zawierajacy pary slowo kluczowe/ilosc wystapien</param>
        /// <returns>Tresc do wyswietlenia</returns>
        static public String generateResponseText(Dictionary<String, Int32> keywordCount)
        {
            StringBuilder response = new StringBuilder();
            foreach (KeyValuePair<String, Int32> a in keywordCount)
            {
                response.Append(String.Format("{0}: {1}\n", a.Key, a.Value));
            }
            return response.ToString();
        }
    }
}
