﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Analiza
{
    /// <summary>
    /// Dostarcza metody do przygotowania oraz analizy adresu strony www
    /// </summary>
    static class UriHelper
    {
        /// <summary>
        /// Tworzy adres Uri na podstawie podanego ciagu znakow
        /// </summary>
        /// <param name="url">Podany ciag znakow</param>
        /// <returns>Adres Uri strony www - null gdy podany adres niepoprawny</returns>
        static public Uri uriCheck(String url)
        {
            Uri resultUri;
            Uri.TryCreate(url,UriKind.Absolute, out resultUri);
            return resultUri;
        }
    }
}
