﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Analiza
{
    /// <summary>
    /// Dostarcza metody do obslugi zadan http
    /// </summary>
    static class WebRequestHelper
    {
        /// <summary>
        /// Pobiera tresc dokumentu html podanej strony www
        /// </summary>
        /// <param name="uri">Adres strony www</param>
        /// <returns>Tresc dokumentu html</returns>
        static public String getPageHtml(Uri uri)
        {
            if (uri == null)
            {
                throw new Exception("Niepoprawny adres.");
            }
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            Stream responseStream = webResponse.GetResponseStream();
            StreamReader siteReader = new StreamReader(responseStream);
            String htmlString = siteReader.ReadToEnd();
            return htmlString;
        }
    }
}
