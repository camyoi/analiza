﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace Analiza
{
    /// <summary>
    /// Dostarcza metody sluzace do przetwarzania oraz analizy dokumentu html 
    /// </summary>
    static class HtmlHelper
    {
        /// <summary>
        /// Pobiera dane slow kluczowych
        /// </summary>
        /// <param name="htmlDoc">Dokument html</param>
        /// <returns>Lista slow kluczowych</returns>
        static public List<String> getKeywordsMetaTagList(HtmlDocument htmlDoc)
        {
            HtmlNode keywordsNode = htmlDoc.DocumentNode.SelectSingleNode("//meta[@name='keywords']");
            if (keywordsNode == null)
            {
                throw new Exception("Brak zdefiniowanych w nagłówku słów kluczowych");
            }
            HtmlAttribute contentAttribute = keywordsNode.Attributes.Where(c => c.Name.Equals("content")).SingleOrDefault();
            List<String> keyWords = contentAttribute.Value.Split(',').ToList();
            return keyWords;
        }

        /// <summary>
        /// Pobiera element body z tresci przekazanego dokumentu html
        /// </summary>
        /// <param name="htmlDoc">Dokument html</param>
        /// <returns>Wezel body</returns>
        static public HtmlNode getBody(HtmlDocument htmlDoc)
        {
            HtmlNode bodyNode = htmlDoc.DocumentNode.SelectSingleNode("//body");
            return bodyNode;
        }

        /// <summary>
        /// Analizuje tresc dokumentu html pod kontem wystepowania slow kluczowych
        /// </summary>
        /// <param name="keyWords">Lista slow kluczowych</param>
        /// <param name="bodyNode">Wezel body dokumentu html</param>
        /// <returns>Slownik zawierajacy pary slowo kluczowe/ilosc wystapien</returns>
        static public Dictionary<String, Int32> countKeys(List<String> keyWords, HtmlNode bodyNode)
        {
            Dictionary<String, Int32>  keywordCount = new Dictionary<String, Int32>();
            foreach (String keyword in keyWords)
            {
                Int32 count = Regex.Matches(bodyNode.InnerText, keyword.Trim()).Count;
                keywordCount.Add(keyword.Trim(), count);
            }
            return keywordCount;
        }

        /// <summary>
        /// Generuje dokument html z tresci dokumentu
        /// </summary>
        /// <param name="html">Tresc dokumentu html</param>
        /// <returns>Dokument html</returns>
        static public HtmlDocument getHtmlDocumentFromHtml(String html)
        {
            HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);
            return htmlDoc;
        }
    }
}
